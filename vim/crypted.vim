

set cursorline
"hi CursorLine ctermbg=Black ctermfg=White
"hi Normal ctermbg=Black ctermfg=Black

function! Hide ()
    hi CursorLine ctermbg=NONE ctermfg=White
    hi Normal ctermbg=Black ctermfg=Black
endfunction
function! Nohide ()
    hi CursorLine ctermbg=NONE ctermfg=White
    hi Normal ctermbg=NONE ctermfg=Gray
endfunction

map h :<C-U>call Hide()<CR>
map H :<C-U>call Nohide()<CR>
set laststatus=2
set statusline=%f\ %=\ (h/H)\ (%v,%l)
call Hide()
call Nohide()
