function! Box_clear ()
    mapclear!
    map <space> :<C-U>call Flip_box()<CR>j^
    imap <C-b> [ ]
    imap <C-t> <todo> </todo>
    imap <C-d> <C-R>=strftime('%y-%m-%d %H:%M')<CR>
    set laststatus=2
    set statusline=%f\ %=\ ([x]\ spc,^bd)\ (%v,%l)
    hi StatusLine ctermfg=cyan ctermbg=black
endfunction
call Box_clear()

function! Flip_box ()
    s!\[ \]![xXx]!eg
    s!\[x\]![ ]!eg
    s!\[xXx\]![x]!eg
    s!<todo>!<todo xXx>!eg
    s!<todo #[^>]*>!<todo>!eg
    s!<todo xXx>!<todo #q>!eg
endfunction

set filetype=markdown
set tabstop=4
