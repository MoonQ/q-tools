" http://www.lingua-systems.com/knowledge/unicode-mappings/cp850-to-unicode.html
" http://www.obliquity.com/computer/html/unicode2500.html

map c1 :call CharDraw_blocks()<CR>
map c2 :call CharDraw_lines()<CR>
map c3 :call CharDraw_doublelines()<CR>
map c4 :call CharDraw_doublehorlines()<CR>
map c5 :call CharDraw_doubleverlines()<CR>
map c6 :call CharDraw_arrows()<CR>
map c7 :call CharDraw_extrachars()<CR>
map c0 :call CharDraw_clear()<CR>
map ck :call CharDraw_noNumPad()<CR>

function! CharDraw_clear ()
    imapclear
    set laststatus=2
    echo 'N to create 80,25 empty drawing. Select ANSI 437 chars and draw with numpad, ck to assign t-m "keypad"'
    map N 80i <Esc>yy24p(
    set statusline=%f\ %=\ \[c1-1:chars\ c0:clr\ ck:key]\ (%v,%l)\ HEX:%B
endfunction
call CharDraw_clear()

function! CharDraw_noNumPad ()
    imap b 1
    imap n 2
    imap m 3
    imap g 4
    imap h 5
    imap j 6
    imap t 7
    imap y 8
    imap u 9
    imap i -
    imap k +
endfunction

function! CharDraw_blocks ()
    imapclear
    imap 1 ░
    imap 2 ▀
    imap 3 ▒
    imap 4 ▐
    imap 5 ■
    imap 6 ▌
    imap 7 ▓
    imap 8 ▄
    imap 9 █
    imap 0  
    imap - ▞
    imap + ▚
    set statusline=%f\ %=\ [░▀▒▐■▌▓▄█ ▞▚]\(%v,%l)\ HEX:%B
endfunction

function! CharDraw_lines ()
    imapclear
    imap 1 └
    imap 2 ┴
    imap 3 ┘
    imap 4 ├
    imap 5 ┼
    imap 6 ┤
    imap 7 ┌
    imap 8 ┬
    imap 9 ┐
    imap - ─
    imap + │
    set statusline=%f\ %=\ [└┴┘├┼┤┌┬┐─│]\(%v,%l)\ HEX:%B
endfunction

function! CharDraw_doublehorlines ()
    imapclear
    imap 7 ╒
    imap 8 ╤
    imap 9 ╕
    imap 4 ╞
    imap 5 ╪
    imap 6 ╡
    imap 1 ╘
    imap 2 ╧
    imap 3 ╛
    imap - ═
    imap + │
    set statusline=%f\ %=\ [╘╧╛╞╪╡╒╤╕═│]\(%v,%l)\ HEX:%B
endfunction

function! CharDraw_doubleverlines ()
    imapclear
    imap 7 ╓
    imap 8 ╥
    imap 9 ╖
    imap 4 ╟
    imap 5 ╫
    imap 6 ╢
    imap 1 ╙
    imap 2 ╨
    imap 3 ╜
    imap + ║
    imap - ─
    set statusline=%f\ %=\ [╙╨╜╟╫╢╓╥╖─║]\(%v,%l)\ HEX:%B
endfunction

function! CharDraw_doublelines ()
    imapclear
    imap 1 ╚
    imap 2 ╩
    imap 3 ╝
    imap 4 ╠
    imap 5 ╬
    imap 6 ╣
    imap 7 ╔
    imap 8 ╦
    imap 9 ╗
    imap - ═
    imap + ║
    set statusline=%f\ %=\ [╚╩╝╠╬╣╔╦╗═║]\(%v,%l)\ HEX:%B
endfunction

function! CharDraw_extrachars ()
    imapclear
    imap 1 ©
    imap 2 ·
    imap 3 ×
    imap 4 «
    imap 5 °
    imap 6 »
    imap 7 ╲
    imap 8 ╳
    imap 9 ╱
    set statusline=%f\ %=\ [©·×«°»╲╳╱]\(%v,%l)\ HEX:%B
endfunction

function! CharDraw_arrows ()
    imapclear
    imap 2 ▼
    imap 4 ◀
    imap 5 ◆
    imap 6 ▶
    imap 8 ▲
    imap - ─
    imap + │
    set statusline=%f\ %=\ [▲▶▼◀◆─│]\(%v,%l)\ HEX:%B
endfunction

"U+254x	╀	╁	╂	╃	╄	╅	╆	╇	╈	╉	╊	╋	╌	╍	╎	╏
"U+255x	═	║	╒	╓	╔	╕	╖	╗	╘	╙	╚	╛	╜	╝	╞	╟
"U+256x	╠	╡	╢	╣	╤	╥	╦	╧	╨	╩	╪	╫	╬	╭	╮	╯
"U+257x	╰	╱	╲	╳	╴	╵	╶	╷	╸	╹	╺	╻	╼	╽	╾	╿
"U+258x ▀   ▁   ▂   ▃   ▄   ▅   ▆   ▇   █   ▉   ▊   ▋   ▌   ▍   ▎   ▏
"U+259x ▐   ░   ▒   ▓   ▔   ▕   ▖   ▗   ▘   ▙   ▚   ▛   ▜   ▝   ▞   ▟
