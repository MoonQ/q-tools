" https://www.dokuwiki.org/wiki:syntax
" h1-6: ====== headings ======
" bb/ii/uu: **bold** //italic// __underline__
" dd: <del>deleted</del>
" l: [[URL|Title]]
" f: ((footnote))
" i/im: {{ wiki:image.png?width }}  (can be url)
" -:  * unordered
" 1:  - ordered list
" c: <code bash file.sh>\n</code>
" t: <todo>

imapclear
set laststatus=2
set statusline=%f\ %=\ \[ctrl-wXX\ heads:h1-6\ bold:bo\ it:it\ undr:un\ del:dd\ \[\[l\]\]\ {{im}}\ bull"-\ enum:1\ code:c]\ (%v,%l)


" ====== headings =======
imap <C-w>h ======  ======<Esc>6hi
imap <C-w>h1 ======  ======<Esc>6hi
imap <C-w>h2 =====  =====<Esc>5hi
imap <C-w>h3 ====  ====<Esc>4hi
imap <C-w>h4 ===  ===<Esc>3hi
imap <C-w>h5 ==  ==<Esc>2hi
imap <C-w>h6 =  =<Esc>1hi

map wh i======  ======<Esc>6hi
map wh1 i======  ======<Esc>6hi
map wh2 i=====  =====<Esc>5hi
map wh3 i====  ====<Esc>4hi
map wh4 i===  ===<Esc>3hi
map wh5 i==  ==<Esc>2hi
map wh6 i=  =<Esc>1hi
" ==== Bold, Italic, Undeline, del ====
imap <C-w>bo ****<Esc>hi
imap <C-w>it ////<Esc>hi
imap <C-w>un ____<Esc>hi
imap <C-w>dd <lt>del><lt>/del><Esc>5hi
" ==== Lists ====
imap <C-w>- <esc>^i<space><space>*<space>
imap <C-b> *<space>
imap <C-w>1 <esc>^i<space><space>-<space>
imap <C-w>, <esc>:s!^    !  !e<CR>l:noh<CR>a
imap <C-w>. <esc>:s!^!  !e<CR>l:noh<CR>a

" ==== Link ====
imap <C-w>l [[\|]]<Esc>2hi
" ==== Footnote ====
imap <C-w>f (())<Esc>1hi
" ==== Image ====
imap <C-w>i {{ :ns:image.jpg?200 \|}}<Esc>19hi
imap <C-w>im {{ :ns:image.jpg?200 \|}}<Esc>19hi
" ==== Code ====
imap <C-w>c <code bash file.sh><CR><CR></code><Esc>ki
map wc i<code bash file.sh><CR><CR></code><Esc>ki

" ==== Todo ====
map wt i<todo><Esc>$a</todo><Esc>
imap <C-t> <todo></todo><Esc>6hi
imap <C-l> * <todo></todo><Esc>6hi
imap <C-w>t <todo></todo><Esc>6hi

imap <C-e> <Esc>$a

map <space> :<C-U>call Flip_todo()<CR>j^
function! Flip_todo ()
    s!<todo>!<todo xXx>!eg
    s!<todo #[^>]*>!<todo>!eg
    s!<todo xXx>!<todo #me>!eg
endfunction

set tabstop=2
set shiftwidth=2
set smartindent
set timeoutlen=1500
