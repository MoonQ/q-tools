#!/bin/bash
_help() {
    echo '
2FA code viewer

Usage:
- Create a (possible encrypted) folder for your service keys
- When the service presents you the QRCode, you typically can
  view the actual code as text. It looks something like:
  I5XXIY3IMEWCA3LBMRSSA6LPOUQGY33PNMQQU===
- Store this piece of text in a file: "Service name".key
- Run 2fa-menu command in the folder

Viewer:
- Codes and service names are printed on the screen

    '

    exit
}

for (( i=1; i<=$#; i++ )); do
  value=${!i}
  j=$(( i + 1 ))
  [[ "$value" = "--help" ]] && _help
  [[ "${value}" = "-"* ]] && {
    [[ "$value" =~ -.*h ]] && { _help; }
    continue
  }
done

which oathtool &> /dev/null || {
  echo Missing oathtool
  exit 1
}
which smenu &> /dev/null || {
  echo Missing smenu
  exit 1
}

timeadjust=0
update_frequency=30

shopt -s nullglob

_exit() {
  clear
  exit
}
_get_code() {
  oathtool -b --totp $( cat "$1" | tr -dc '[:print:]' )
}
_print_codes() {
    echo "Refresh"
    even=0
    for k in *key; do
      name=${k%.key}
      name=${name//_/ }
      printf "%-${PAD_LENGTH}s [ %s ] \n" \
        "$name" \
        $( _get_code "$k" )
      even=$(( 1 - $even ))
    done
}


while true; do
    clear
    PAD_LENGTH=0
    for key in *key; do
      name=${key%.key}
      if [[ ${#name} -gt $PAD_LENGTH ]]; then
        PAD_LENGTH=${#name}
      fi
    done

    set -e
    new_codes=$( _print_codes )
    set +e
    codes=$new_codes
    i=$(( ( $( date +%s ) - $timeadjust ) % $update_frequency ))
    left=$(( $update_frequency - $i ))
    when=$( date -d "now + $left seconds" +"%H:%M:%S" )
    selected=$( echo -e "$codes" | smenu \
        -M -m "Codes change in ${left}s at $when" -W $'\n' \
        -t 1 -n 30 -a c:0/2 i:3 \
    )

    if [[ -z "$selected" ]]; then
      exit
    fi
    if [[ "$selected" = "Refresh" ]]; then
       continue
    fi
    code=$( echo "$selected" | rev | cut -d" " -f2 | rev )
    echo Copying $code to clipboard
    echo -n $code | xclip -selection clipboard 2> /dev/null
    echo -n $code | xclip 2> /dev/null
    countdown -n  15
    exit
done
