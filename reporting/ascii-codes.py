#!/usr/bin/env python3
import sys, os

sys.path.append(os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "..", "py-packages", "markslider", "markslider"
))
import ansicodes
from argparse import ArgumentParser


def setup_options():
    parser = ArgumentParser()
    parser.add_argument("-v", default=False, action="store_true")
    parser.add_argument(
        "cs",
        type=str,
        default="cp437",
        nargs="?",
        help="Character set to show, ex. ascii, cp437, cp850, cp1250, latin1 ...",
    )
    opts = parser.parse_args()
    return opts


def long_format(table):
    bc = ansicodes.code()
    for x, col in enumerate(table):
        for y, row in enumerate(col):
            bc.posprint(
                y + 2, 1 + x * 7, bc.color_string("${c}%03d: ${G}%s${Z}" % (row))
            )


def short_format(table):
    bc = ansicodes.code()
    for x, col in enumerate(table):
        for y, row in enumerate(col):
            bc.posprint(x + 3, y * 2 + 1, bc.color_string("${G}%s${Z}" % (row[1],)))
    print("")


if __name__ == "__main__":
    opts = setup_options()
    table = [[] for x in range(8)]
    for c in range(0, 256):
        col = int(c / 32)
        try:
            table[col].append(
                (c, chr(c).encode('latin1').decode(opts.cs))
            )
        except UnicodeDecodeError:
            table[col].append(
                (c, " ")
            )
        # sys.stdout.write(chr(c).decode('latin1'))
    # remove control chars
    table.pop(0)
    bc = ansicodes.code()
    bc.clear()
    if opts.v:
        lines = int(25 - len(opts.cs) / 2 - 2)
    else:
        lines = int(32 - len(opts.cs) / 2 - 2)
    print(bc.color_string("=" * lines + " ${Y}%s${Z} " % (opts.cs,) + "=" * lines))
    if opts.v:
        long_format(table)
    else:
        short_format(table)
    print("")
