#!/usr/bin/env python3
import sys
import os
import pwd
from argparse import ArgumentParser

MINSIZE = 0


def setup_options():
    parser = ArgumentParser(description="List file usage")
    parser.add_argument("startpath", action="append", nargs="+")

    options = parser.parse_args()
    return options


def add_recurse(db, options, start):
    for path, dirs, files in os.walk(start, followlinks=False):
        files = clean_syms(files, path)
        for f in files:
            filename = os.path.join(path, f)
            stats = os.stat(filename)
            owner = stats.st_uid
            size = stats.st_size
            if not owner in db:
                db[owner] = 0
            db[owner] += size
    return db


def clean_syms(files, path):
    nonsyms = []
    for f in files:
        if not os.path.islink(os.path.join(path, f)):
            nonsyms.append(f)
    return nonsyms


def print_db(db):
    namedb = dict()
    for u in db:
        try:
            namedb[pwd.getpwuid(u).pw_name] = humanize_size(db[u])
        except:
            namedb[str(u)] = humanize_size(db[u])
    names = list(namedb.keys())
    names.sort()
    for n in names:
        print(n + ": " + namedb[n])


def humanize_size(size, precision=1):
    if size == None:
        return "nan"
    suffixes = ["B", "KB", "MB", "GB", "TB"]
    suffixIndex = 0
    defPrecision = 0
    while size > 1024:
        suffixIndex += 1  # increment the index of the suffix
        size = size / 1024.0  # apply the division
        defPrecision = precision
    return "%.*f%s" % (defPrecision, size, suffixes[suffixIndex])


def main():
    options = setup_options()
    db = dict()
    for f in options.startpath[0]:
        db = add_recurse(db, options, f)
    print_db(db)
    sys.exit(0)


main()
