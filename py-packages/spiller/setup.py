from distutils.core import setup
import os


def version_reader(path):
    for line in open(path, "rt").read(1024).split("\n"):
        if line.startswith("__version__"):
            return line.split("=")[1].strip().replace('"', "")


version = version_reader(os.path.join("spiller", "__init__.py"))

setup(
    name="spiller",
    packages=["spiller"],
    version=version,
    description="Very simple password storage, that encrypts with GPG cmdline tool.",
    author="Ville Rantanen",
    author_email="q@six9.net",
    entry_points={
        "console_scripts": [
            "spill = spiller.spiller:main",
        ]
    },
)
