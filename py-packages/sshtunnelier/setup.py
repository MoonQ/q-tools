import os
from distutils.core import setup


def version_reader(path):
    for line in open(path, "rt").read(1024).split("\n"):
        if line.startswith("__version__"):
            return line.split("=")[1].strip().replace('"', "")


version = version_reader(os.path.join("sshtunnelier", "__init__.py"))

setup(
    name="sshtunnelier",
    packages=["sshtunnelier"],
    version=version,
    description="SSH tunnel manager (yet another)",
    author="Ville Rantanen",
    author_email="q@six9.net",
    entry_points={
        "console_scripts": [
            "ssh-tunnelier = sshtunnelier:main",
        ]
    },
    install_requires=[
        "psutil", "pyyaml"
    ],
)
