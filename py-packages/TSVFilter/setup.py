import os
from distutils.core import setup


def version_reader(path):
    for line in open(path, "rt").read(1024).split("\n"):
        if line.startswith("__version__"):
            return line.split("=")[1].strip().replace('"', "")


version = version_reader(os.path.join("TSVFilter", "filter.py"))
setup(
    name="TSVFilter",
    packages=["TSVFilter"],
    version=version,
    description="TSV column filter.",
    author="Ville Rantanen",
    author_email="ville.q.rantanen@gmail.com",
    keywords=["TSV", "data"],
    entry_points={
        "console_scripts": [
            "TSVFilter=TSVFilter:main",
        ],
    },
)
