from distutils.core import setup
import os


def version_reader(path):
    for line in open(path, "rt").read(1024).split("\n"):
        if line.startswith("__version__"):
            return line.split("=")[1].strip().replace('"', "")


version = version_reader(os.path.join("ffmpegparser", "ffmpegparser.py"))
setup(
    name="ffmpegparser",
    packages=["ffmpegparser"],
    version=version,
    description="View parse ffmpeg and ffprobe output nicer",
    author="Ville Rantanen",
    author_email="ville.q.rantanen@gmail.com",
    keywords=["ffmpeg"],
    entry_points={
        "console_scripts": [
            "ffmpeg-parser = ffmpegparser.ffmpegparser:main",
            "ffprobe-parser = ffmpegparser.ffprobeparser:main",
        ]
    },
    install_requires=["parse", "ansi"],
)
