# Python library for ansi colorization
import re, sys


class code:
    K = "\033[1;30m"
    R = "\033[1;31m"
    G = "\033[1;32m"
    B = "\033[1;34m"
    Y = "\033[1;33m"
    M = "\033[1;35m"
    C = "\033[1;36m"
    W = "\033[1;37m"

    k = "\033[0;30m"
    r = "\033[0;31m"
    g = "\033[0;32m"
    b = "\033[0;34m"
    y = "\033[0;33m"
    m = "\033[0;35m"
    c = "\033[0;36m"
    w = "\033[0;37m"

    bk = "\033[40m"
    br = "\033[41m"
    bg = "\033[42m"
    by = "\033[43m"
    bb = "\033[44m"
    bm = "\033[45m"
    bc = "\033[46m"
    bw = "\033[47m"

    S = "\033[1m"  # strong
    s = "\033[2m"  # strong off
    U = "\033[4m"  # underline
    u = "\033[24m"  # underline off
    Z = "\033[0m"  # zero colors
    ic = "\033[7m"  # inverse colors
    io = "\033[27m"  # inverse off
    st = "\033[9m"  # strike on
    so = "\033[29m"  # strike off
    CLR = "\033[2J"
    CLREND = "\033[K"
    CLRBEG = "\033[1K"
    CLRSCR = CLR + "\033[0;0H"

    color_keys = "K,R,G,B,Y,M,C,W,k,r,g,b,y,m,c,w,S,s,U,u,Z,ic,io,st,so,bk,br,bg,by,bb,bm,bc,bw,CLR,CLREND,CLRBEG,CLRSCR".split(
        ","
    )
    color_list = [
        K,
        R,
        G,
        B,
        Y,
        M,
        C,
        W,
        k,
        r,
        g,
        b,
        y,
        m,
        c,
        w,
        S,
        s,
        U,
        u,
        Z,
        ic,
        io,
        st,
        so,
        bk,
        br,
        bg,
        by,
        bb,
        bm,
        bc,
        bw,
        CLR,
        CLREND,
        CLRBEG,
        CLRSCR,
    ]
    custom_match = re.compile(r"(\${)([0-9;]*[ABCDEFGHJKSTfminsu]+)(})")

    def pos(self, y, x):
        """Go to absolute position"""
        return "\033[" + str(y) + ";" + str(x) + "H"

    def column(self, x):
        """Go to absolute column"""
        return "\033[" + str(x) + "G"

    def posprint(self, y, x, s):
        """Print string at a location"""
        sys.stdout.write(self.pos(y, x) + str(s))
        self.flush()

    def clear(self):
        sys.stdout.write(self.CLRSCR + self.pos(0, 0))

    def clear_to_end(self):
        sys.stdout.write(self.CLREND)

    def clear_to_beginning(self):
        sys.stdout.write(self.CLRBEG)

    def up(self, n=1):
        sys.stdout.write("\033[" + str(n) + "A")

    def down(self, n=1):
        sys.stdout.write("\033[" + str(n) + "B")

    def right(self, n=1):
        sys.stdout.write("\033[" + str(n) + "C")

    def left(self, n=1):
        sys.stdout.write("\033[" + str(n) + "D")

    def up_line(self, n=1):
        sys.stdout.write("\033[" + str(n) + "F")

    def down_line(self, n=1):
        sys.stdout.write("\033[" + str(n) + "E")

    def save(self):
        """Save cursor position"""
        sys.stdout.write("\033[s")

    def restore(self):
        """Restore cursor position"""
        sys.stdout.write("\033[u")

    def color_string(self, s):
        for i, c in enumerate(self.color_keys):
            s = s.replace("${" + c + "}", self.color_list[i])
        return self.custom_color(s)

    def nocolor_string(self, s):
        for i, c in enumerate(self.color_keys):
            s = s.replace("${" + c + "}", "")
        return self.custom_nocolor(s)

    def custom_color(self, s):
        return self.custom_match.sub("\033[\\2", s)

    def custom_nocolor(self, s):
        return self.custom_match.sub("", s)

    def get_keys(self):
        return self.color_keys

    def flush(self):
        sys.stdout.flush()


def demo():
    """Print all control sequences"""
    c = code()
    unformatted = """${S}ANSI CODES
==========${Z}
${S}Fo${U}rm${st}at${u}ti${ic}ng${Z}
${S}==========${Z}
   0  Z Clear format
   1  S ${S}Strong   ${Z}     2  s ${s}Off${Z}
   4  U ${U}Underline${Z}    24  u Off
   7 ic ${ic}Inverse${Z}      27 io Off
   9 st ${st}Strike${Z}       29 so Off
${R}Co${G}lo${B}rs${Z}
${S}======${Z}
  30 k ${k}Black   ${Z}1 K ${K}Strong   ${Z}40 bk ${bk}Background${Z}
  31 r ${r}Red     ${Z}1 R ${R}Strong   ${Z}41 br ${br}Background${Z}
  32 g ${g}Green   ${Z}1 G ${G}Strong   ${Z}42 bg ${bg}Background${Z}
  33 y ${y}Yellow  ${Z}1 Y ${Y}Strong   ${Z}43 by ${by}Background${Z}
  34 b ${b}Blue    ${Z}1 B ${B}Strong   ${Z}44 bb ${bb}Background${Z}
  35 m ${m}Magenta ${Z}1 M ${M}Strong   ${Z}45 bm ${bm}Background${Z}
  36 c ${c}Cyan    ${Z}1 C ${C}Strong   ${Z}46 bc ${bc}Background${Z}
  37 w ${w}White   ${Z}1 W ${W}Strong   ${Z}47 bw ${bw}Background${Z}
${S}Clearing and movement
${S}=====================${Z}
   2J     CLR     Clear screen
   2J ;H  CLRSCR .clear()  Clear screen and cursor to upper left
   K      CLREND  Clear to end of line
   1K     CLRBEG  Clear to beginning of line

   s .save()  Save location   u .restore()   Restore location
   A .up()    Up              E .up_line()   Up line
   B .down()  Down            F .down_line() Down line
   C .right() Right        y;xH .pos()       Absolute Position
   D .left()  Left """

    return c.color_string(unformatted)
