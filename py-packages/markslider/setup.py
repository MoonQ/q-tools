import os
from distutils.core import setup


def version_reader(path):
    for line in open(path, "rt").read(1024).split("\n"):
        if line.startswith("__version__"):
            return line.split("=")[1].strip().replace('"', "")


version = version_reader(os.path.join("markslider", "markslider.py"))
setup(
    name="markslider",
    packages=["markslider"],
    version=version,
    description="View markdown files as slides in terminal",
    author="Ville Rantanen",
    author_email="ville.q.rantanen@gmail.com",
    keywords=["Markdown", "Slideshow"],
    entry_points={"console_scripts": "markslider = markslider.markslider:main"},
    install_requires=["pygments", "climage"],
)
