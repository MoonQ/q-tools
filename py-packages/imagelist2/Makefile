SHELL := /bin/bash
.PHONY:
.ONESHELL:
help: ## *:･ﾟ✧*:･ﾟ✧ This help *:･ﾟ✧*:･ﾟ✧
	@printf "\033[36;1m  %14s  \033[0;32;1m %s\033[0m\n" Target Description
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk ' \
			BEGIN {FS = ":.*?## "}; \
			{ if ( $$1 != "-") { \
			    printf "\033[31;1m[ \033[36;1m%14s \033[31;1m]\033[0;32;1m %s\033[0m\n", $$1, $$2 \
			   } else { \
			    printf "               \033[0;33;1m=^= %-25s =^=\033[0m\n", $$2 \
			  } \
			} \
		'

-: ## Building

all: clean install test

install: ## Run installer
	set -e
	. useve-runner
	useve imagelist2
	py-format ./
	pip install ./

PRINT_TABLE := 'sqlite3 -header image-list.sqlite "SELECT * FROM files" | tabulate -1 -s "\|"'
PRINT_DATA := 'sqlite3 -header image-list.sqlite "SELECT * FROM data" | tabulate -1 -s "\|"'
PRINT_LIST := 'sqlite3 -header image-list.sqlite "SELECT * FROM list" | tabulate -1 -s "\|"'

test: test-db test-du test-dup test-tag ## Test

test-db:
	set -e
	. useve-runner
	useve imagelist2
	echo =================================
	mkdir -p folder1/folder2 folder1/.hidden folder1/_med
	convert -size 600x300 xc:red red.jpg
	cp red.jpg folder1/.hidden/
	convert -size 600x300 xc:cyan folder1/cyan.jpg
	convert -size 600x300 xc:cyan folder1/cyan.png
	cp folder1/cyan.png folder1/cyan_dup2.png
	cp folder1/cyan.png folder1/cyan_dup3.png
	cp folder1/cyan.jpg folder1/cyan_dup2.jpg
	cp folder1/cyan.jpg folder1/cyan_dup3.jpg
	convert -size 600x300 plasma: folder1/noisy.png
	convert -size 600x300 plasma: -blur 0x3 folder1/blur.png
	convert -size 300x600 xc:blue folder1/folder2/blue.tif
	convert wizard: folder1/wizard.jpg
	convert folder1/wizard.jpg -resize 95%x98% folder1/wizard.mod.jpg
	convert folder1/wizard.jpg -flip -resize 95%x98% folder1/wizard.flip.jpg
	image-list db -x imagelist2
	eval ${PRINT_TABLE}
	convert -size 600x600 xc:black folder1/black.png
	image-list db -x imagelist2
	eval ${PRINT_TABLE}
	rm folder1/black.png
	image-list db -x imagelist2
	eval ${PRINT_TABLE}
	mogrify -rotate 90 folder1/cyan.png
	image-list db -x imagelist2 -c
	eval ${PRINT_LIST}
	eval ${PRINT_DATA}
	image-list db -x imagelist2
	eval ${PRINT_DATA}
	image-list db -x imagelist2 --measure
	eval ${PRINT_DATA}
	eval ${PRINT_TABLE}
	echo "========= check sha1 ============="
	sqlite3 image-list.sqlite  "SELECT hash,file FROM list" -separator '  ' | sha1sum -c -


test-du:
	set -e
	. useve-runner
	useve imagelist2
	echo =================================
	image-list du
	echo =================================
	image-list du -d 2
	echo =================================
	image-list du -d 1
	echo =================================
	image-list du -d 1 folder1/

test-dup:
	set -e
	. useve-runner
	useve imagelist2
	echo ========== duplicates =======================
	image-list search --dup
	echo ========== visual duplicates =======================
	image-list search --visdup
	echo ========== nearest red ======================
	image-list search --color 255,0,0,10
	echo ========== nearest from file ======================
	image-list search --color red.jpg,3
	echo ========== Similar by dhash ======================
	image-list search --similar 30
	echo ========== Similar by file ======================
	image-list search --similar folder1/wizard.jpg

test-tag:
	set -e
	. useve-runner
	useve imagelist2
	echo ========== tag add =======================
	image-list tag -t plain -t red red.jpg
	image-list tag -t red red.jpg
	echo ========== tag list =======================
	image-list tag red.jpg
	echo ========== tag delete =======================
	image-list tag -d red red.jpg
	echo ========== tag list fail =======================
	image-list tag red.jpg.missing || true


init: ## Init test env
	. useve-runner
	useve mk imagelist2

clean: ## Clean testfiles
	rm -rf folder1 image-list.sqlite red.jpg || true
