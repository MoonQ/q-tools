import os
from distutils.core import setup


def version_reader(path):
    for line in open(path, "rt").read(1024).split("\n"):
        if line.startswith("__version__"):
            return line.split("=")[1].strip().replace('"', "")


version = version_reader(os.path.join("imagelist2", "__init__.py"))
setup(
    name="imagelist2",
    packages=["imagelist2"],
    version=version,
    description="Maintains a list of images sqlite file",
    author="Ville R",
    author_email="q@six9.net",
    keywords=["images"],
    entry_points={
        "console_scripts": [
            "image-list = imagelist2:main",
        ]
    },
    install_requires=["PyTurboJPEG", "Pillow", "ImageHash", "numpy", "tqdm"],
)
