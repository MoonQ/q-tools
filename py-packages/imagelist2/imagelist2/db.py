import os
import sqlite3
from math import sqrt as sqlite_sqrt


class DB:
    def __init__(self, sqlfile):
        self.sqlfile = sqlfile
        self.root_path = os.path.dirname(os.path.realpath(sqlfile))
        self.create_db()
        self.connect()

    def create_db(self):

        if os.path.exists(self.sqlfile):
            return

        conn = sqlite3.connect(self.sqlfile)
        db = conn.cursor()
        conn.text_factory = str
        db.execute(
            """CREATE TABLE data (
                hash TEXT PRIMARY KEY,
                description TEXT,
                portrait BOOLEAN,
                width INTEGER,
                height INTEGER,
                fingerprint TEXT,
                sharpness NUMERIC,
                R REAL, G REAL, B REAL, BR REAL, BG REAL, BB REAL
            )"""
        )
        db.execute("CREATE TABLE list (file TEXT PRIMARY KEY,hash TEXT,date INTEGER,size INTEGER)")
        db.execute("CREATE TABLE tags (hash TEXT,tag TEXT)")
        db.execute(
            """CREATE VIEW files AS
              SELECT list.file, list.date, list.size, data.*
              FROM list
              LEFT JOIN data ON data.hash = list.hash"""
        )
        db.execute("CREATE UNIQUE INDEX data_hash ON data(hash)")
        db.execute("CREATE UNIQUE INDEX list_file ON list(file)")

        conn.commit()
        return

    def connect(self):
        conn = sqlite3.connect(self.sqlfile)
        conn.text_factory = str
        conn.create_function("RELATIVE", 1, self.file2relative)
        self.conn = conn

        return conn

    def cursor(self):
        return self.conn.cursor()

    def get_folder_contents(self, path):
        """return the contents of the folder"""
        files = []
        res = self.cursor().execute("SELECT file FROM list where file LIKE ?", (f"{path}%",))
        for row in res:
            base = row[0].replace(path, "", 1)
            if not "/" in base:
                files.append(row[0])
        return files

    def is_time_mismatch(self, image):
        count = (
            self.cursor()
            .execute(
                "SELECT COUNT(1) FROM list WHERE file = ? AND date = ?",
                (
                    image.filename,
                    image.get_time(),
                ),
            )
            .fetchall()[0][0]
        )
        return count == 0

    def is_hash_mismatch(self, image):
        count = (
            self.cursor()
            .execute(
                "SELECT COUNT(1) FROM list WHERE file = ? AND hash = ?",
                (
                    image.filename,
                    image.get_hash(),
                ),
            )
            .fetchall()[0][0]
        )
        return count == 0

    def hash2file(self, hash):

        return [
            row[0]
            for row in self.cursor()
            .execute(
                "SELECT file FROM LIST WHERE hash = ?",
                (hash,),
            )
            .fetchall()
        ]

    def file2hash(self, file):

        try:
            return [
                row[0]
                for row in self.cursor()
                .execute(
                    "SELECT hash FROM LIST WHERE file = ?",
                    (file,),
                )
                .fetchall()
            ][0]
        except Exception:
            return None

    def file2relative(self, file):

        return os.path.relpath(file, self.root_path)


def sqlite_square(x):
    return x * x
