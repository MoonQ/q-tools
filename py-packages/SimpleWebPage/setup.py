import os
from distutils.core import setup


def version_reader(path):
    for line in open(path, "rt").read(1024).split("\n"):
        if line.startswith("VERSION"):
            return line.split("=")[1].strip().replace('"', "")


version = version_reader(os.path.join("simplewebpage", "__init__.py"))
setup(
    name="SimpleWebPage",
    packages=["simplewebpage"],
    version=version,
    description="HTML file index generator.",
    author="Ville Rantanen",
    author_email="ville.q.rantanen@gmail.com",
    keywords=["HTML", "generator"],
    entry_points={
        "console_scripts": [
            "SimpleWebPage=simplewebpage:main",
        ],
    },
    install_requires=["markdown>=3.3.4"],
)
