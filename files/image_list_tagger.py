#!/usr/bin/env python3
import sys,os
from Tkinter import *
from PIL import Image,ImageTk, ImageDraw
import math
from argparse import ArgumentParser
import sqlite3

SQLFILE='list_of_images.sqlite'
IMGMATCH=re.compile('.*\.jpg$|.*\.jpeg$|.*\.png$|.*\.gif$',re.I)

def setup_options():
    parser=ArgumentParser(description="Tag images in image_list")
    parser.add_argument("-f",action="store",dest="sqlfile",default=SQLFILE,
                      help="SQL file name to use [%(default)s]")
    parser.add_argument("-t",action="store",dest="tags",default="thisIsGood,thisIsBad",
                      help="Comma separated list of tags")
    parser.add_argument("-n",action="store",type=int,dest="number",default=0,
                      help="Number of tags expected. 0 is any amount.")
    parser.add_argument('path', action="store",default='.', nargs='?')

    options=parser.parse_args()
    options.tags=[x.strip() for x in options.tags.split(",")]
    return options


class Click:
    def __init__(self, image,tags,sqlfile,title,number):

        self.title=title
        self.root=Tk()
        self.root.geometry("1024x768+220+0")
        self.root.bind("<Escape>", self._quit)
        self.root.title(self.title)

        self.numberLimit=number
        self.numberCurrent=1
        self.image = Image.open(image)
        self.image_name = image
        self.img_copy= self.image.copy()
        self.pixels = self.img_copy.load()
        self.background_image = ImageTk.PhotoImage(self.image)

        self.background = Label(self.root, image=self.background_image, cursor='cross')
        self.background.pack(fill="both", expand=True, side="left")
        self.background.bind('<Configure>', self._resize_image)

        self.top=Toplevel(self.root)
        self.top.bind("<Escape>", self._quit)
        self.tagTexts=tags
        self.tags=[]
        self.tags.append(Button(self.top,text="[z] Next", command=self._tag_button("z")))
        self.root.bind("z", self._tag_key)
        for t in range(len(self.tagTexts)):
            return_func=self._tag_button(t+1)
            if t<34:
                self.tags.append(Button(self.top,text="[%s] %s"%(self._hotkey(t+1),self.tagTexts[t]), command=return_func))
                self.root.bind(self._hotkey(t+1), self._tag_key)
                sys.stdout.write("\n%s: %s"%( self._hotkey(t+1), self.tagTexts[t]) )
                #self.top.bind(str(t), self._tag_key)
                continue
            self.tags.append(Button(self.top,text=self.tagTexts[t], command=return_func))
        for t in range(len(self.tags)):
            self.tags[t].pack(fill="both", side="top")
        # custom tag
        self.tags.append(Entry(self.top))
        self.tags[t+1].pack(fill="both", side="top")
        self.tags[t+1].bind("<Return>", self._tag_key)
        self.tags.append(Button(self.top, text="Custom", command=self._tag_button("custom")))
        self.tags[t+2].pack(fill="both", side="top")

        self.top.geometry("220x%d+0+0"%(self.root.winfo_screenheight()-150,))

        self._resize_init()

        self._init_db(sqlfile)
        self._print_tags()

        self.root.mainloop()

    def _tag_key(self,event):
        if event.keysym=="z":
            self._tag_button("z")()
            return
        if event.keysym=="Return":
            self._tag_button("custom")()
            return
        thisfunc=self._tag_button(self._yektoh(event.keysym))
        thisfunc()

    def _tag_button(self,n):
        def actual_value(x=n):
            if x=="z":
                self._next(n)
                return
            self._add_tag(x)
        return actual_value

    def _resize_image(self,event):
        new_width = event.width
        new_height = event.height
        self.image = self.img_copy.resize((new_width, new_height))
        self.background_image = ImageTk.PhotoImage(self.image)
        self.background.configure(image =  self.background_image)

    def _resize_init(self):
        event = self._get_max_size()
        self.root.geometry("%dx%d+220+0"% (event.width, event.height))
        self._resize_image(event)

    def _get_max_size(self,refer_size=None):
        """ return max size for image that fits the refer_size,.
            otherwise, return max size for the screen """
        if refer_size:
            refer_width=refer_size[0]
            refer_height=refer_size[1]
        else:
            refer_width=int(float(self.root.winfo_screenwidth()-220))
            refer_height=self.root.winfo_screenheight()-150
        new_height=refer_height
        new_width= int(float(self.img_copy.size[0])/float(self.img_copy.size[1])*new_height)
        if new_width>refer_width:
            new_width=refer_width
            new_height=int(float(self.img_copy.size[1])/float(self.img_copy.size[0])*new_width)
        event = type('eventclass', (object,),
                 {'width':new_width, 'height':new_height})()
        return event

    def _quit(self,event):
        self.root.destroy()
        sys.exit(0)

    def _next(self,event):
        self.root.destroy()

    def _add_tag(self,x):
        if x=="custom":
            value=self.tags[ len(self.tags)-2 ].get().strip()
            if value not in self.tagTexts:
                self.tagTexts.append(value)
            if value=="":
                return
        else:
            value=self.tagTexts[x-1]
        self.db[0].execute("SELECT hash FROM list WHERE file = ?",( os.path.realpath( self.image_name), ))
        hashes=self.db[0].fetchall()
        if len(hashes)==0:
            print("Image %s not in database!" % (self.image_name) )
            self._next(None)
            return
        self.db[0].execute("INSERT INTO tags (hash,tag) VALUES (?,?)",( hashes[0][0],value  ))
        self.db[1].commit()
        print("Added %s:%s"%(self.image_name,value))
        self.numberCurrent+=1
        if self.numberLimit>0 and self.numberLimit<self.numberCurrent:
            self._next(0)
        return

    def _print_tags(self):
        self.db[0].execute("SELECT hash FROM list WHERE file = ?",( os.path.realpath( self.image_name), ))
        hashes=self.db[0].fetchall()
        if len(hashes)==0:
            print("Image %s not in database!" % (self.image_name) )
            self._next(None)
            return
        self.db[0].execute("SELECT tag FROM tags WHERE hash = ?",( hashes[0][0], ))

        print("tags: "+",".join([x[0] for x in self.db[0]]))
        return

    def _init_db(self, sqlfile):
        if not os.path.exists(sqlfile):
            print("Cannot find SQLite file: "+sqlfile)
            sys.exit(1)
        conn=sqlite3.connect(sqlfile)
        conn.text_factory=str
        db=conn.cursor()
        self.db=[db,conn]

    def get_tags(self):
        return self.tagTexts

    def _hotkey(self,i):
        # return string number for 1-9
        #        a-l if i>9
        if i<10:
            return str(i)
        a=i+87
        if a<122:
            return chr(a)
        return
    def _yektoh(self,a):
        # return integer, reverse function of _hotkey
        i=ord(a)-87
        if i<0:
            return int(a)
        return i

opt=setup_options()
if os.path.isfile(opt.path):
    imagelist=[opt.path]
else:
    imagelist=sorted([os.path.join(opt.path,f) for f in os.listdir(opt.path) if IMGMATCH.match(f)])

for i,l in enumerate(imagelist):
    (p,b)=os.path.split( os.path.abspath(l) )
    (f,p)=os.path.split( p )
    sys.stdout.write("%s/%s %d/%d "%( p,b, i+1,len(imagelist)))
    clicker = Click(l,opt.tags,opt.sqlfile,"Tagger: %s/%s %d/%d "%( p,b, i+1,len(imagelist)),opt.number)
    #print(clicker.get_tags())
