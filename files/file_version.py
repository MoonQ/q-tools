#!/usr/bin/env python3
import os, argparse, sys
from shutil import copyfile, copytree
from datetime import datetime

def get_options():
    parser = argparse.ArgumentParser(
        description = 'Rename file with version number'
    )
    parser.add_argument(
        '-c',
        action = "store_false",
        dest = "move",
        default = True,
        help = "Copy path instead of moving",
    )
    parser.add_argument(
        '-d',
        action = "store_true",
        dest = "date",
        help = "Use paths date+time as version number",
        default = False
    )
    parser.add_argument(
        '-f',
        action = "store_true",
        dest = "force",
        help = "Force overwrite",
        default = False
    )
    parser.add_argument(
        '-q',
        action = "store_true",
        dest = "quiet",
        help = "Quiet operation",
        default = False
    )
    parser.add_argument(
        '-n',
        action = "store_true",
        dest = "no_action",
        help = "Do not modify files, just print the new file name.",
        default = False
    )
    parser.add_argument(action = "store", dest = "file")
    return parser.parse_args()


def get_version_name(full_path):
    """ New name versioned with integer  """
    file_dir = os.path.dirname(full_path)
    file_name = os.path.basename(full_path)
    basename, extension = os.path.splitext(file_name)
    version = 1

    while True:
        new_name = os.path.join(
            file_dir,
            "%s.v%02d%s"%(
                basename,
                version,
                extension
            )
        )
        if os.path.exists(new_name):
            version += 1
        else:
            break
    return new_name


def get_date_name(full_path):
    """ New name versioned with date of the file  """
    file_dir = os.path.dirname(full_path)
    file_name = os.path.basename(full_path)
    basename, extension = os.path.splitext(file_name)
    file_time =  os.stat(full_path).st_mtime
    time_formatted = datetime.fromtimestamp(
        file_time
    ).strftime("%Y%m%d_%H%M%S")

    new_name = os.path.join(
        file_dir,
        "%s.%s%s"%(
            basename,
            time_formatted,
            extension
        )
    )

    if full_path.endswith("%s%s"%(
        time_formatted,
        extension
    )):
        # file is already named with the date+time
        return full_path
    return new_name


def test_existing(name, overwrite):
    if os.path.exists(name):
        if overwrite:
            os.remove(name)
        else:
            sys.stderr.write(
                "'%s' already exists, exiting.\n"%(
                    name
                )
            )
            sys.exit(1)


if __name__ == "__main__":
    opts = get_options()
    if opts.date:
        new_name = get_date_name(opts.file)
    else:
        new_name = get_version_name(opts.file)
    if not opts.quiet:
        print(new_name)
    if opts.no_action:
        sys.exit(0)
    test_existing(new_name, opts.force)
    if opts.move:
        os.rename(opts.file, new_name)
    else:
        if os.path.isdir(opts.file):
            copytree(opts.file, new_name)
        else:
            copyfile(opts.file, new_name)
