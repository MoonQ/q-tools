#!/bin/bash

set -e
function helpexit() {
    echo Delete files with a progress bar.
    echo This command is always recursive to folders!
    echo '-f       Do not ask questions'
    echo '--nc     Do not count files first'
    echo '--shred  Use "shred -u" instead of rm. WARNING: modifies hardlinked files!'
    exit
}

FORCE=0
RMCOMMAND="rm --interactive=never"
FOLDERS=( )
for ((i=1; i<=${#@}; i++)) {
    [[ "${!i}" = "-h" ]] && helpexit
    [[ "${!i}" = "--help" ]] && helpexit
    [[ "${!i}" = "-f" ]] && { FORCE=1; RMFORCE="-f"; continue; }
    [[ "${!i}" = "--nc" ]] && { NOCOUNT=1; continue; }
    [[ "${!i}" = "--shred" ]] && { RMCOMMAND="shred -u"; continue; }
    [[ "${!i}" = "-"* ]] && helpexit
    FOLDERS+=( "${!i%/}" )
}
[[ "${#FOLDERS[@]}" -eq 0 ]] && helpexit

function listfiles() {
    while IFS= read -r -d $'\0' line; do
        files=$((files+1))
        printf "\r%02d:%02d:%02d %d" $(($SECONDS/3600)) $(($SECONDS%3600/60)) $(($SECONDS%60)) $files
    done < <(find "$@" \( -type f -or -type l \) -print0)
    printf "\r"
}
function deletefiles() {
    i=0
    done_update=-10
    left_update=0
    filespad=${#files}
    timeleft="  :  :  "
    printf "\r%8s %8s %${filespad}s%${filespad}s %3s %s\n" \
        Time Left " " Files " " Name
    while IFS= read -r -d $'\0' line; do
        # Print progress
        i=$((i+1))
        if [[ $(( $SECONDS - $done_update )) -ge 1 ]]; then
          done_update=$SECONDS
          done=$SECONDS
          [[ $files -ne 0 ]] && percent=$((200*$i/$files % 2 + 100*$i/$files))
          printf -v timedone "%02d:%02d:%02d" \
            $(($done/3600)) $(($done%3600/60)) $(($done%60))
        fi
        if [[ $files -ne 0 ]]; then
            if [[ $(( $SECONDS - $left_update )) -ge 10 ]]; then
              left_update=$SECONDS
              left=$(( ($files - $i) * $SECONDS / $i ))
              left_live=$left
            else
              left_live=$(( $left - $SECONDS + $left_update ))
            fi
            if [[ -n "$left" ]]; then
                printf -v timeleft "%02d:%02d:%02d" $(($left_live/3600)) $(($left_live%3600/60)) $(($left_live%60))
            fi
        fi
        printf "\r%s %s %${filespad}d/%${filespad}d %2d%% %s\033[0K" \
           "$timedone" "$timeleft" "$i" "$files" "$percent" "$line"

        # Delete file
        if [ -L "$line" ]; then
            rm $RMFORCE "$line"
        else
            $RMCOMMAND $RMFORCE "$line"
        fi
    done < <(find "$@" \( -type f -or -type l \) -print0)
    printf "\r%s %s %${filespad}d/%${filespad}d %2d%%" \
       "$timedone" "00:00:00" "$i" "$files" "100"
    printf "\n"
}
function listfolders() {
   while IFS= read -r -d $'\0' line; do
        folders=$((folders+1))
        printf "\r%02d:%02d:%02d %d" $(($SECONDS/3600)) $(($SECONDS%3600/60)) $(($SECONDS%60)) $folders
    done < <(find "$@" -type d -print0)
    printf "\r"
}
function deletefolders() {
    i=0
    while IFS= read -r -d $'\0' line; do
        i=$((i+1))
        [[ $folders -ne 0 ]] && percent=$((200*$i/$folders % 2 + 100*$i/$folders))
        printf "\r%02d:%02d:%02d %6d/%d %3d%% %s\033[0K" \
           $(($SECONDS/3600)) $(($SECONDS%3600/60)) $(($SECONDS%60)) $i $folders "$percent" "$line"
        rm $RMFORCE -r --interactive=never "$line"
    done < <(find "$@" -depth -type d -print0)
    printf "\n"
}

# return line wrapping
trap "printf '\033[?7h'" 1 9 15
files=0
folders=0
if [[ "$NOCOUNT" -ne 1 ]]; then
  echo Listing files in "${FOLDERS[@]}" ...
  # stop line wrapping
  printf '\033[?7l'
  listfiles "${FOLDERS[@]}"
  echo Prepared to delete $files files
fi
# stop line wrapping
printf '\033[?7l'
if [[ $FORCE -eq 0 ]]; then
  echo '<ctrl-c> to quit'
  read foo
fi
SECONDS=0
deletefiles "${FOLDERS[@]}"
if [[ "$NOCOUNT" -ne 1 ]]; then
  listfolders "${FOLDERS[@]}"
  echo Removing remaining $folders folders
fi
deletefolders "${FOLDERS[@]}"
# return line wrapping
printf '\033[?7h'
