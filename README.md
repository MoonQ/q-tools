# Shell Tools package

```
           _____ ___
          |_   _/ _ \
            | || (_) |
            |_| \__\_\
```

This is a growing selection of shell tools used by me and my colleagues.

Each tool is an individual program, which do often depend on other tools.
Dependencies are best found by running the programs.

To see the help page of each tool, run the ./README script

# Installation

## Dependencies

Many of the tools use common unix tools. Some extra tools are required. Here are
the installation commands.

```
sudo apt-get install git sqlite3 python3 python3-magic python3-pip python3-gtk2 sc \
  gnuplot wget vim rsync curl pv
pip2 install csvkit ncsv

```

## Installation without repository

```
mkdir -p ~/lib/q-tools
curl https://bitbucket.org/MoonQ/q-tools/get/HEAD.tar.gz | tar xz --strip 1 -C ~/lib/q-tools
source ~/lib/q-tools/rc
```
Add the to your startup scripts:
`echo '. ~/lib/q-tools/rc' >> ~/.bashrc`

## Installation in user home

```
mkdir -p ~/lib
git clone https://bitbucket.org/MoonQ/q-tools ~/lib/q-tools
echo '. ~/lib/q-tools/rc' >> ~/.bashrc
```


## Systemwide installation

```
git clone https://bitbucket.org/MoonQ/q-tools /usr/local/share/q-tools
echo '[ -e /usr/local/share/q-tools/rc ] && . /usr/local/share/q-tools/rc' >> /etc/profile.d/q-tools.sh
ln -s /usr/local/share/q-tools/rc  /etc/cron.weekly/q-tools
```

The last step ensures weekly updates


## Python modules

Markslider is a terminal slide presentation with markdown backend:

- `pipx install https://bitbucket.org/MoonQ/q-tools/raw/HEAD/py-packages/markslider.tgz`

TSVFilter: a TSV (or CSV) filtering with simple syntax. Limit printed rows by matching header names and operators, ex. "Column1>0.5,Column2==10"

- `pipx install https://bitbucket.org/MoonQ/q-tools/raw/HEAD/py-packages/TSVFilter.tgz`

SimpleWepPage: An HTML file index generator

- `pipx install https://bitbucket.org/MoonQ/q-tools/raw/HEAD/py-packages/SimpleWebPage.tgz`
