#!/usr/bin/env python3
#
# Copyright 2017 Ville Rantanen
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

'''SC to TSV convert.'''

__author__ = "Ville Rantanen"

__version__ = "0.1"

import sys
from argparse import ArgumentParser
import unicodedata, re
import subprocess

def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

class SCReader:
    """ Class for reading SC files.

    """
    def __init__(self,data):
        self.data=data.split('\n')
        self.parserre=re.compile('.* ([A-Z]+)([0-9]+) = (.*)')

    def _parse_row(self,string):
        col=None
        row=None
        content=None
        m=self.parserre.match(string.strip())
        if m:
            col=self.alpha_to_column(m.group(1))
            row=self.try_int(m.group(2))
            content=m.group(3)
        return col,row,content

    def try_int(self,string):

        try:
            return int(string)
        except:
            return string

    def alpha_to_column(self,alpha):
        ''' Returns a column number from spreadsheet column alphabet '''
        n=0
        o=0
        for char in alpha[::-1]:
            o+=(ord(char.upper())-64)*(26**n)
            n+=1
        return int(o-1)

    def next(self):
        ''' Returns the next row in the table, three items: column, row, content'''
        return self._parse_row(self.reader.next())

    def __iter__(self):
        for row in self.data:
            yield self._parse_row(row)

def setup_options():
    ''' Setup the command line options '''

    parser=ArgumentParser()
    parser.add_argument("-v","--version",action='version', version=__version__)
    parser.add_argument("-d",type=str,dest="delimiter",default="\t",
                      help="Delimiter for the TSV, default: [tab]")
    parser.add_argument("sc",type=str,action="store",
                      help="SC file to convert")
    options=parser.parse_args()
    return options

def tsv_write(screader,fileout,delim):
    ''' writes a TSV from SCReader iterator '''
    content=[]
    rows=0
    cols=0
    for row in screader:
        if row[0]!=None:
            content.append(row)
            rows=max(row[1],rows)
            cols=max(row[0],cols)
    table=[]
    for r in range(rows+1):
        table.append([])
        for c in range(cols+1):
            table[r].append('')
    for e in content:
        table[e[1]][e[0]]=e[2]
    for row in table:
        fileout.write(delim.join(row)+'\n')

if not which('sc'):
    print('You don\'t seem to have "sc" installed!')
    print('sudo apt-get install sc')
    sys.exit(1)

opts=setup_options()
proc=subprocess.Popen(['sc','-v','-P','%',opts.sc], stdout=subprocess.PIPE)
sc=proc.stdout.read()
sc_reader=SCReader(sc)
tsv_write(sc_reader,sys.stdout,opts.delimiter)



