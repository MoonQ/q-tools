#!/bin/bash

# Start me to print all the helps to 
# all the commands within this repository


cd $( dirname $0 )/bin

(
echo '
      _____ ___  
     |_   _/ _ \ 
       | || (_) |
       |_| \__\_\
    '
echo Add the tools to PATH by sourcing the "rc" in this folder
echo Update the package with:_q-tools-update

echo -e "\n===== COMMANDS IN qcd_function ====="
echo -e "===================================="

grep "^function " -A 1 ../qcd_function 


echo -e "\n===== COMMANDS IN bin/ ====="
echo -e "============================"
for e in *; do
   test -x $e || continue
   echo -e "\n===== $e =====\n"
   [[ $e = fastdu ]] && { ./$e --help; continue; }
   ./$e -h
done

) | less

